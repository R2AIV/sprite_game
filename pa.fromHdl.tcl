
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name sprite_sp3 -dir "D:/Xilinx/Proj/sprite_sp3/planAhead_run_1" -part xc3s500epq208-4
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "sprite_top.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {spr_mem.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {syncgen.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {sprite_16x16.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {sprinte_top.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top sprite_top $srcset
add_files [list {sprite_top.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc3s500epq208-4
