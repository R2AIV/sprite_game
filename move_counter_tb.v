`timescale 1ns / 1ps


module move_counter_tb;

	// Inputs
	reg clk50;

	// Outputs
	wire [11:0] Pos;

	// Instantiate the Unit Under Test (UUT)
	move_counter uut (
		.clk50(clk50), 
		.Pos(Pos)
	);

	initial begin
		// Initialize Inputs
		clk50 = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
	
	always #5 clk50 = ~clk50;
      
endmodule

