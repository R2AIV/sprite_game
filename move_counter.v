`timescale 1ns / 1ps


module move_counter(
    input clk50,
    output reg [11:0] Pos,
	 output reg clk10hz
    );

reg [17:0] div_counter;
//reg clk10hz;

initial
begin
	div_counter = 0;
	clk10hz = 0;
	Pos = 0;	
end

always @(posedge clk50)
begin
	if(div_counter == 18'd125000) 
	begin
		div_counter <= 18'd0;
		clk10hz <= ~clk10hz;
	end
	else div_counter <= div_counter + 18'd1;
end

always @(posedge clk10hz)
begin
	if(Pos < 12'd40) Pos <= 12'd40;
	else if(Pos >= 12'd744) Pos <= 12'd40;
	else Pos <= Pos + 12'd1;
end

endmodule
