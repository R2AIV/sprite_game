`timescale 1ns / 1ps

module sprite_tb;

	// Inputs
	reg [11:0] PosX;
	reg [11:0] PosY;
	reg [11:0] CurrentX;
	reg [11:0] CurrentY;
	reg clk50;
	reg active;

	// Outputs
	wire r;
	wire g;
	wire b;

	// Instantiate the Unit Under Test (UUT)
	sprite_16x16 uut (
		.PosX(PosX), 
		.PosY(PosY), 
		.CurrentX(CurrentX), 
		.CurrentY(CurrentY), 
		.clk50(clk50), 
		.active(active), 
		.r(r), 
		.g(g), 
		.b(b)
	);

	initial begin
		// Initialize Inputs
		PosX = 10;
		PosY = 10;
		CurrentX = 0;
		CurrentY = 0;
		clk50 = 0;
		active = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		#20 CurrentX = 9; CurrentY = 13;
		#20 CurrentX = 10; CurrentY = 13;
		#20 CurrentX = 11; CurrentY = 13;
		#20 CurrentX = 12; CurrentY = 13;
		#20 CurrentX = 13; CurrentY = 13;
		#20 CurrentX = 14; CurrentY = 13;
		#20 CurrentX = 15; CurrentY = 13;
		#20 CurrentX = 16; CurrentY = 13;
		#20 CurrentX = 17; CurrentY = 13;
		#20 CurrentX = 18; CurrentY = 13;
		#20 CurrentX = 19; CurrentY = 13;
		#20 CurrentX = 20; CurrentY = 13;
		#20 CurrentX = 21; CurrentY = 13;
		#20 CurrentX = 22; CurrentY = 13;
		#20 CurrentX = 23; CurrentY = 13;
		#20 CurrentX = 24; CurrentY = 13;
		#20 CurrentX = 25; CurrentY = 13;
		#20 CurrentX = 26; CurrentY = 13;
		#20 CurrentX = 27; CurrentY = 13;
		#20 CurrentX = 28; CurrentY = 13;
		#20 CurrentX = 29; CurrentY = 13;
		
		#20 CurrentX = 9; CurrentY = 14;
		#20 CurrentX = 10; CurrentY = 14;
		#20 CurrentX = 11; CurrentY = 14;
		#20 CurrentX = 12; CurrentY = 14;
		#20 CurrentX = 13; CurrentY = 14;
		#20 CurrentX = 14; CurrentY = 14;
		#20 CurrentX = 15; CurrentY = 14;
		#20 CurrentX = 16; CurrentY = 14;
		#20 CurrentX = 17; CurrentY = 14;
		#20 CurrentX = 18; CurrentY = 14;
		#20 CurrentX = 19; CurrentY = 14;
		#20 CurrentX = 20; CurrentY = 14;
		#20 CurrentX = 21; CurrentY = 14;
		#20 CurrentX = 22; CurrentY = 14;
		#20 CurrentX = 23; CurrentY = 14;
		#20 CurrentX = 24; CurrentY = 14;
		#20 CurrentX = 25; CurrentY = 14;
		#20 CurrentX = 26; CurrentY = 14;
		#20 CurrentX = 27; CurrentY = 14;
		#20 CurrentX = 28; CurrentY = 14;
		#20 CurrentX = 29; CurrentY = 14;
        
		// Add stimulus here

	end
      
endmodule

