`timescale 1ns / 1ps


module sprite_top(
    input clk50,
	 input sw2,
	 input sw3,
    output [3:0] vga_r,
    output [3:0] vga_g,
    output [3:0] vga_b,
    output vga_hsync,
    output vga_vsync
    );
	 
wire [11:0] CounterX;
wire [11:0] CounterY;
wire [11:0] PosX;

reg [11:0] PosY;

wire r;
wire g;
wire b;

wire inDisplayArea;

wire clk10hz;

assign vga_r[0] = r;
assign vga_r[1] = (r | (CounterX[3:0] == 5)) & inDisplayArea;
assign vga_r[2] = r;
assign vga_r[3] = r;

assign vga_g[0] = g;
assign vga_g[1] = (g | (CounterY[3:0] == 5)) & inDisplayArea;
assign vga_g[2] = g;
assign vga_g[3] = g;

assign vga_b[0] = b;
assign vga_b[1] = b;
assign vga_b[2] = b;
assign vga_b[3] = b;
	 
initial PosY = 12'd300;

always @(posedge clk10hz) 
begin
	if(~sw2) PosY <= PosY - 1;
	if(~sw3) PosY <= PosY + 1;
end

hvsync_generator vga_sync (
    .clk(clk50), 
    .vga_h_sync(vga_hsync), 
    .vga_v_sync(vga_vsync), 
    .inDisplayArea(inDisplayArea), 
    .CounterX(CounterX), 
    .CounterY(CounterY)
    );

sprite_16x16 sprite (
    .PosX(PosX), 
    .PosY(PosY), 
    .CurrentX(CounterX), 
    .CurrentY(CounterY), 
    .clk50(clk50), 
    .active(active), 
    .r(r), 
    .g(g), 
    .b(b)
    );

move_counter Position_X (
    .clk50(clk50), 
    .Pos(PosX),
	 .clk10hz(clk10hz)
    );


endmodule
